package Pages;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class homePage {

    @FindBy(css = "div >button[aria-label='Akkoord']")
    public WebElement agreementBtn;
    @FindBy(css = "div[class='footer__dpgmedia-copy']")
    public WebElement footerAddress;
    @FindBy(css = "iframe[title=\"SP Consent Message\"]")
    public WebElement iFrameWebElement;

    public static final String HOME_PAGE_URL = "https://www.hln.be/";
    public static final String ADDRESS_TO_VERIFY = "Mediaplein 1, 2018 Antwerpen";
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public homePage(WebDriver driver) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

}
