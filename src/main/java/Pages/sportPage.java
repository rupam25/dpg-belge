package Pages;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.util.List;

public class sportPage {

    @FindBy(css = "div.dfp-space")
    public List<WebElement> adsSection;

    public static final String SPORT_PAGE_URL = "https://www.hln.be/sport/";
    final Logger log = LoggerFactory.getLogger(this.getClass());

    public sportPage(WebDriver driver) {
        log.debug("Messages page init");
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
}
