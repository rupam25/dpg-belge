package uiTestScripts;

import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import org.openqa.selenium.*;
import Pages.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import BaseService.*;

public class hlnWebPageTests extends baseServices {
    WebDriver driver;
    final Logger log = LoggerFactory.getLogger(hlnWebPageTests.class);

    @BeforeMethod
    public void beforeMethod() {
        log.debug("Launching browser");
        driver = getDriver("chrome");
    }

    @Test(description = "TS-1 Validate Specific String in Address section")
    public void validateAddress() {
        homePage hPage = new homePage(driver);
        log.debug("Access application url");
        driver.get(hPage.HOME_PAGE_URL);
        waitForXMillis(2000);
        WebElement frameElement = hPage.iFrameWebElement;
        driver.switchTo().frame(frameElement);
        SoftAssert soft = new SoftAssert();

        soft.assertTrue(hPage.agreementBtn.isDisplayed(), "Agreement button is available");
        log.debug("Click on Akkoord button");
        hPage.agreementBtn.click();

        waitForXMillis(2000);
        log.debug("Scroll down till bottom of web page");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");

        waitForXMillis(1000);

        soft.assertTrue(hPage.footerAddress.getText().contains(hPage.ADDRESS_TO_VERIFY), "Address has specific string present in it");
        soft.assertAll();
    }


    @Test(description = "TS-2 Verify presence of Advertisement")
    public void verifyAdsPresence() {
        homePage hPage = new homePage(driver);
        sportPage sPage = new sportPage(driver);

        log.debug("Access application url");
        driver.get(sPage.SPORT_PAGE_URL);
        waitForXMillis(500);

        WebElement frameElement = hPage.iFrameWebElement;
        driver.switchTo().frame(frameElement);
        SoftAssert soft = new SoftAssert();
        log.debug("Click on Akkoord button");
        hPage.agreementBtn.click();

        waitForXMillis(500);
        soft.assertTrue(sPage.adsSection.size() > 0, "Atleast one advertisement is present");
        soft.assertAll();
    }

    @AfterMethod
    public void afterMethod() {
        log.debug("closing browser");
        driver.quit();
    }
}



